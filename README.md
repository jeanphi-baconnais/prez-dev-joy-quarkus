Pré requis :
    - sdk use java 11.0.9.j9-adpt
    - sdk use maven 3.6.3
    - vérif quarkus -version

# 🗣 Présentation Dev Joy Quarkus 


# Quarkus CLI 

## Commandes à montrer 

`quarkus create app talk-quarkus --extension=resteasy,resteasy-jsonb`

Montrer qu'on peut créer un projet en kotlin avec cette commande 
`quarkus create app talk-quarkus --kotlin --extension=resteasy`

```
quarkus build
quarkus extension list
quarkus extension list --concise -i -s swagger
quarkus extension list -i
quarkus extension add swagger
quarkus dev 
```

Montrer l'équivalent en maven rapidement pour montrer l'apport de la CLI :
```
mvn quarkus:list-extensions
mvn compile quarkus:dev
``` 

Création de projets via mvn 

```
mvn io.quarkus:quarkus-maven-plugin:2.0.3.Final:create \
    -DprojectGroupId=org.zenika.talks \
    -DprojectArtifactId=talk-quarkus \
    -DclassName="org.zenika.talks.SupervisionResource" \
    -Dextensions="cache,resteasy-jsonb"
```

via l'extension vscode

via https://code.quarkus.io/

# Quarkus Dev mode

- Démarrage de l’application avec endpoint basique : `quarkus dev`
- (vérifier que le continious testing est bien en pause)
- parler du code start RESTeasy 
- [Appel depuis le navigateur](http://localhost:8080/hello)
- Mise à jour de la réponse (classe GreetingResource)
- [Appel depuis le navigateur](http://localhost:8080/hello)
- Montrer le redémarrage
- Modifier la conf et l'ajouter dans le retour de la classe GreetingResource
  `vars.hello = DevFest Lille`

```
 @ConfigProperty(name = "vars.hello")
 private String hello;
```

- Rajouter une extension dans le pom.xml (swagger) : `quarkus ext add swagger`
- Montrer le redémarrage
- Montrer la [console de dev](http://localhost:8080/q/dev) et accéder à l'[ihm swagger](http://localhost:8080/q/swagger-ui/)


# Console Dev UI 
- Montrer la console
- Parler des extensions et de leur guide


- Création d'un ParamBean, générer constructor (vide et avec les 2 params) et getter / setter  et stopper aux dev services 

```
public class ParamBean {

    private String key;

    private String value;
}

```



# Quarkus DevService

- Evoquer MongoPanache
- Ajouter dépendance : `quarkus ext add quarkus-mongodb-panache`
- Vérifier la console quarkus et la présence de téléchargement d'image mongo (voir un docker ps).
- Ajouter l'héritage `extends PanacheMongoEntity` de sur la classe ParamBean
- Ajouter l'annotation `@MongoEntity(collection="params")` sur la classe ParamBean
- Ajouter le endpoint dans la classe 
```
@GET
@Path("/params")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public List<ParamBean> getAllParam() {
    return ParamBean.listAll();
}
```

- Ajouter les propriétés d'accès à la BDD
```
quarkus.mongodb.connection-string = mongodb://localhost:27017
quarkus.mongodb.database = prez
```
- Ajouter le @PostConstruct dans la classe GreetingResource
```
    @PostConstruct
    public void initBDD() {
        new ParamBean("Key1", "Value1").persist();
        new ParamBean("Key2", "Value2").persist();
        new ParamBean("Key3", "Value3").persist();
        new ParamBean("Key4", "Value4").persist();
    }


    @PreDestroy
    public void destroy() {
        ParamBean.deleteAll();
    }
```

- Ajouter la ressource [params](http://localhost:8080/hello/params) ou via swagger

# Quarkus Continous testing

- Visualiser les raccourcis présents en fin de console
- Visualiser la page [dev ui](http://localhost:8080/q/dev)
- Reactiver les tests depuis la console (comme la ressource REST a été modifiée, le test doit être KO)
- Modifier la classe de test pour avoir le bon retour de testé.
- Vérifier que le test est bien OK

``` 
    @Test
    public void testHelloParamEndpoint() {
        given()
                .when().get("/hello/params")
                .then()
                .statusCode(200)
                .body(is("Hello DevFest Lille !"));
    }
```


# Quarkus Remote dev

Préparation :
```
gcloud auth login <mail> // pour s'authentifier
gcloud config set project quarkus-devjoy-talk

mvn clean package -Dquarkus.package.type=mutable-jar 
gcloud app deploy 

```
lancer un quarkus remote dev
Ouvrir l’index.html de GCP
Faire scanner un QR code => voir les canards 
dans le fichier properties, commenter / décommenter la var “image” et voir les impacts 


# Bonus 

Ajouter l’extension kubernetes : `quarkus ext add kubernetes` et montrer le fichier à la racine du projet GitLab.



# Commandes en vrac

Lister les dépendances : ```/usr/local/maven/apache-maven-3.6.3/bin/mvn quarkus:list-extensions``` ou `quarkus ext list`

Rechercher une dépendance : `quarkus ext list --concise -i -s swagger`

Installation d'une dépendance : `quarkus ext add quarkus-smallrye-openapi`

+ install jsonb : `quarkus ext add quarkus-resteasy-jsonb` 


# Connexion à une bdd et utilisation de la lib Panache

Ajouter ces params dans le application.properties :

```
quarkus.mongodb.connection-string = mongodb://localhost:27017
quarkus.mongodb.database = prez
```

Création d'un Bean `ParamBean` :

```

@MongoEntity(collection="params")
public class ParamBean extends PanacheMongoEntity {

    private String key;

    private String value;
    
```

Création d'une ressource MongoDBResource avec un :

```

@Path("/params")
public class MongoDBResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<ParamBean> getAllParam() {
        return ParamBean.listAll();
    }
}
```

Copier / coller la ressource d'ajout du ParamBean :

```
@POST
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public Response addParam(ParamBean newParam) {
    newParam.persist();
    return Response.status(Response.Status.CREATED).build();
}
```


## 🕸  Graphql 

Ajouter une dépendance : 
`mvn quarkus:add-extension -Dextensions="smallrye-graphql"` 
ou `quarkus ext add smallrye-graphql`.

Création d'une classe GraphQLResource (@GraphQLApi) contenant une requete 

```
    
    @Query("getAllParameters")
    @Description("Get all parameters")
    public List<ParamBean> getAllParameters() {
        return ParamBean.listAll();
    }
    
```

curl http://localhost:7777/graphql/schema.graphql

http://localhost:7777/graphql-ui/

```
query {
  getAllParameters {
    id,
    value
  }
}
```



## 🐳 Docker :

```
mvn package
classique : docker build -t prez-dev-joy-quarkus:1.0-SNAPSHOT -f src/main/docker/Dockerfile.jvm .
```

Vérifier la présence de l'image : `docker images | grep prez`

Exécuter l'image : `docker run prez-dev-joy-quarkus:1.0-SNAPSHOT`


```
mvn package -Pnative
native : docker build -t prez-dev-joy-quarkus-native -f src/main/docker/Dockerfile.native .
```

## 🐋 Déploiement kubernetes

L'image a été ajoutée dans le repo GitLab sinon on fait faire un :

```
docker push registry.gitlab.com/jeanphi-baconnais/prez-dev-joy-quarkus
```

Ajouter une dépendance : ```mvn quarkus:add-extension -Dextensions="kubernetes"```  ou `quarkus ext add kubernetes`

Build : ```mvn package```

Appliquer le yaml : ```k apply -f target/kubernetes/kubernetes.yml```

(si regénération du yaml, modifier :
- l'image registry.gitlab.com/jeanphi-baconnais/prez-dev-joy-quarkus
- service => LoadBalancer
- imagePullPolicy: IfNotPresent
  )

```
k get pods
k get deploy
k get svc

curl http://localhost:7777/hello
```
    
# Remote dev

Commande de packaging: 
````
mvn clean package -Dquarkus.package.type=mutable-jar
````


Construction de l'image
````
 docker build -f src/main/docker/Dockerfile.dev -t quarkus-remote-joy .
````


Lancement du mode de dev remote: 

````
mvn quarkus:remote-dev -Ddebug=false \
  -Dquarkus.package.type=mutable-jar \
  -Dquarkus.live-reload.url=http://quarkus-devjoy-talk.ew.r.appspot.com \
  -Dquarkus.live-reload.password=123

````
