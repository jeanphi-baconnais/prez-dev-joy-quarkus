package org.zenika.talks;

import io.smallrye.mutiny.Multi;

import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.jboss.resteasy.annotations.SseElementType;

@Path("/prez")
public class Resource {

    private static int nbDuck = 0;

    @Singleton
    private List<String> listDucks = new ArrayList<>();

    @ConfigProperty(name = "image")
    private String image;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public Response incrementVisitors() {
        this.nbDuck++;
        return Response.ok()
                .build();
    }

    @POST
    @Path("/reset")
    @Produces(MediaType.TEXT_PLAIN)
    public Response resetVisitor() {
        this.nbDuck = 0;
        return Response.ok()
                .build();
    }

    @GET
    @Path("/compteur")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getNbVisitor() {
        return Response.ok(this.nbDuck).build();
    }

    @GET
    @Path("/reactif/compteur")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    @SseElementType(MediaType.APPLICATION_JSON)
    public Multi<Integer> getNbVisitorReactif(){
        return Multi.createFrom().item(() -> this.nbDuck);
        //return this.visitor;
    }

    @GET
    @Path("/image")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getImage() {
        return Response.ok(this.image).build();
    }

    @GET
    @Path("/reactif/image")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    @SseElementType(MediaType.APPLICATION_JSON)
    public Multi<String> getImageReactive(){
        return Multi.createFrom().item(() -> this.image);
    }
}
