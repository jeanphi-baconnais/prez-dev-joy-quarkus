package org.zenika.talks;

import io.quarkus.mongodb.panache.PanacheMongoEntity;
import io.quarkus.mongodb.panache.common.MongoEntity;

@MongoEntity(collection="params")
public class ParamBean extends PanacheMongoEntity {

    private String key;

    private String value;

    public ParamBean(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public ParamBean() {
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
