package org.zenika.talks;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

@Path("/hello")
public class GreetingResource {

    @ConfigProperty(name = "vars.hello")
    private String world;

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String hello() {
        return "Hello world !";
    }

    @GET
    @Path("/params")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public List<ParamBean> getAllParam() {
        return ParamBean.listAll();
    }

    @PostConstruct
    public void initBDD() {
        new ParamBean("Key1", "Value1").persist();
        new ParamBean("Key2", "Value2").persist();
        new ParamBean("Key3", "Value3").persist();
        new ParamBean("Key4", "Value4").persist();
    }


    @PreDestroy
    public void destroy() {
        ParamBean.deleteAll();
    }


}
